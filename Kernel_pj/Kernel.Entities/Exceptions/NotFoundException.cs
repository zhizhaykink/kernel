﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kernel.Entities.Exceptions
{
	public class NotFoundException : Exception
	{
		private string Message { get; set; }
		public NotFoundException(string message = null) : base(message) => Message = message;
	}
}
