﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kernel.Entities.Exceptions
{
	public class NotRoleException : Exception
	{
		private string Message { get; set; }
		public NotRoleException(string message = null) : base(message) => Message = message;
	}
}
