﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kernel.Entities.Exceptions
{
	public class RoleUndefinedException : Exception
	{
		private string Message { get; set; }
		public RoleUndefinedException(string message = null) : base(message) => Message = message;
	}
}
