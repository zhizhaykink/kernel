﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kernel.Entities.Models
{
	public class Token
	{
		public int ID { get; set; }
		public int? ID_User { get; set; }
		public string? Roles { get; set; }
		public string GUID { get; set; }
		public TimeSpan? Live { get; set; }
	}
}
