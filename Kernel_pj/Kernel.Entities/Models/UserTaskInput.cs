﻿namespace TestOfCamundaClass.Handler
{
	public class UserTaskInput
	{
		public string Action { get; set; }
		public int ID_Pair { get; set; }
		public string ProcessInstanceID { get; set; }
	}
}