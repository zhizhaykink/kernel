﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kernel.Entities.Services
{
	public class MapperService<TIn, TOut>
	{
		public TOut Execute(TIn obj)
		{
			var config = new MapperConfiguration(cfg => cfg.CreateMap<TIn, TOut>());
			var mapper = new Mapper(config);
			return mapper.Map<TIn, TOut>(obj);
		}
	}
}
