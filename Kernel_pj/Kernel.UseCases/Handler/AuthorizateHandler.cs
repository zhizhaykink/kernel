﻿
using EF.Services.Implementation;
using Kernel.Entities;
using Kernel.Entities.Exceptions;
using Kernel.Entities.Models;
using Kernel.Entities.Services;
using Persistence.Services;
using Persistence.Services.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kernel.UseCases.Handler
{
	public class AuthorizateHandler
	{
		private readonly TokenService service = new(new CoreContextes.Models.CoreContext());
		private readonly StudentService studentService = new(new CoreContextes.Models.CoreContext());
		private readonly OrganizationService organizationService = new(new CoreContextes.Models.CoreContext());
		private readonly SupervisorService supervisorService = new(new CoreContextes.Models.CoreContext());
		private readonly RoleService roleService = new(new CoreContextes.Models.CoreContext());

		public Token Get(string guid)
		{
			var tmpToken = service.GetByGUID(guid);
			if (tmpToken is null)
			{
				throw new UnAuthorizateException();
			}
			var item = new MapperService<EF.Token, Token>().Execute(tmpToken);
			if (tmpToken.ID_Role is null)
			{
				throw new RoleUndefinedException();
			}
			item.Roles = roleService.GetByID((int)tmpToken.ID_Role).Name;
			return item;
		}

		public void Delete(string guid) => service.Delete(guid);
		public string Create(Accaunt accaunt)
		{
			var role = roleService.GetByName(accaunt.Role);
			if (role is null)
			{
				throw new NotRoleException("Role undefined");
			}
			IActor user;
			user = role.Name switch
			{
				null => throw new NotRoleException("Role undefined"),
				"Student" => studentService.GetByLogin(new MapperService<Accaunt, EF.Student>().Execute(accaunt)),
				"Supervisor" => supervisorService.GetByLogin(new MapperService<Accaunt, EF.Supervisor>().Execute(accaunt)),
				"Administrator" => organizationService.GetByLogin(accaunt.Login),
				_ => throw new RoleUndefinedException("Role incorrected")
			};
			//var user = studentService.GetByLogin(accaunt.Login);
			if (user == null || user.Pass != accaunt.Pass)
			{
				throw new NotUserException("Invalid login or password. Please check inputting data on correct");
			}
			EF.Token token = new();
			token.ID_Role = role.ID;
			token.ID_User = user.ID;
			var time = DateTime.Now.AddHours(1);
			token.Live = new TimeSpan(time.Day, time.Hour, time.Minute, time.Second, time.Millisecond);
			token.GUID = Guid.NewGuid().ToString().Replace("-", "");
			service.Insert(token);
			return token.GUID;
		}
	}
}
