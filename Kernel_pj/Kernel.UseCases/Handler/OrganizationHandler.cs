﻿using EF;
using EF.Services.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kernel.UseCases.Handler
{
	public class OrganizationHandler
	{
		private readonly OrganizationService service = new(new CoreContextes.Models.CoreContext());

		public void Add(Organization organization) => service.Insert(organization);

		public Organization Get(Organization organization) => service.GetByID(organization.ID);

		public object GetBranches(Organization organization)
		{
			// тут нужно сформировать массив объектов и искать по ID_Parent
			return service.GetByID(organization.ID);
		}

		public void Delete(Organization organization) => service.Delete(organization.ID);

		public void Update(Organization organization)
		{
			if (organization.Login is not null )	
				service.UpdateLogin(organization.ID, organization.Login);
			if (organization.Email is not null)
				service.UpdateEmail(organization.ID, organization.Email);
			if (organization.Pass is not null)
				service.UpdatePass(organization.ID, organization.Pass);
			if (organization.Director is not null)
				service.UpdateDirector(organization.ID, organization.Director);
			if (organization.Name is not null)
				service.UpdateName(organization.ID, organization.Name);
		}
	}
}
