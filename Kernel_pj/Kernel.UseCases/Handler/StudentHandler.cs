﻿using EF;
using EF.Services.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kernel.UseCases.Handler
{
	public class StudentHandler
	{
		private readonly StudentService service = new(new CoreContextes.Models.CoreContext());
		public void Add(Student student) => service.Insert(student);
		public Student Get(Student student) => service.GetByID(student);


		public void Delete(Student student) => service.Delete(student);

		public void Update(Student student)
		{
			if (student.Login is not null)
				service.UpdateLogin(student);
			if (student.Email is not null)
				service.UpdateEmail(student);
			if (student.Pass is not null)
				service.UpdatePass(student);
			if (student.FullName is not null)
				service.UpdateName(student);
		}
	}
}
