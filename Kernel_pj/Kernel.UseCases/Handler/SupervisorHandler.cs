﻿using EF;
using EF.Services.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kernel.UseCases.Handler
{
	public class SupervisorHandler
	{
		private readonly SupervisorService service = new(new CoreContextes.Models.CoreContext());
		public void Add(Supervisor supervisor) => service.Insert(supervisor);
		public Supervisor Get(Supervisor supervisor) => service.GetByID(supervisor);


		public void Delete(Supervisor supervisor) => service.Delete(supervisor);

		public void Update(Supervisor supervisor)
		{
			if (supervisor.Login is not null)
				service.UpdateLogin(supervisor);
			if (supervisor.Email is not null)
				service.UpdateEmail(supervisor);
			if (supervisor.Pass is not null)
				service.UpdatePass(supervisor);
			if (supervisor.FullName is not null)
				service.UpdateName(supervisor);
		}
	}
}
