﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Kernel.Entities.Exceptions;
using Kernel.Entities.Models;
using Kernel.UseCases.Handler;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Kernel
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        public string Roles { get; set; }
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var roles = Roles.Replace(" ", "").Split(',');
            AuthorizateHandler handler = new();
            Token user = new();
            var token = context.HttpContext.Request.Headers["Token"];
            if (token == "" )
			{
                throw new UnAuthorizateException();
			}
            if ((user = handler.Get(context.HttpContext.Request.Headers["Token"])) is null)
			{
                throw new UnAuthorizateException();
            }
            if (!roles.Contains(user.Roles))
			{
                throw new MemberAccessException();
			}
            context.HttpContext.Items.Add("role", user.Roles);
            context.HttpContext.Items.Add("user", user.ID_User);
        }
    }
}