﻿using AutoMapper;
using Kernel.Entities;
using Kernel.Entities.Exceptions;
using Kernel.Entities.Services;
using Kernel.Models.Authorizate;
using Kernel.UseCases;
using Kernel.UseCases.Handler;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Kernel.Controllers
{
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class AuthorizateController : ControllerBase
	{
		private readonly AuthorizateHandler handler = new();
		[HttpPost]
		[ActionName("ToAuth")]
		public object Authorization([FromBody] AuthorizateModel authorizate)
		{
			var item = new MapperService<AuthorizateModel, Accaunt>().Execute(authorizate);
			var token = handler.Create(item);
			this.Response.StatusCode = 200;
			this.Response.Headers.Add("Token", token);
			return new { Message = "Authorizated" };
		}

		[HttpPost]
		[ActionName("UnAuth")]
		public object UnAuthorization()
		{
			if (this.HttpContext.Request.Headers["Token"] == "")
			{
				throw new UnAuthorizateException();
			}
			handler.Delete(this.HttpContext.Request.Headers["Token"]);
			this.HttpContext.Response.StatusCode = 200;
			return new { Message = "UnAuthorizated" };
		}
	}
}
