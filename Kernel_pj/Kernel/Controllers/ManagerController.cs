﻿using EF;
using EF.Services.Implementation;
using Kernel.Entities.Exceptions;
using Kernel.Entities.Models;
using Kernel.Entities.Services;
using Kernel.Models;
using Microsoft.AspNetCore.Mvc;
using Persistence.Services.Implementation;
using System.Text.Json;
using TestOfCamundaClass;
using TestOfCamundaClass.Handler;

namespace Kernel.Controllers
{
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class ManagerController : ControllerBase
	{

		[HttpPost]
		[ActionName("StartProcess")]
		[Authorize(Roles = "Administrator")]
		public object StartProcess([FromQuery] StartProcessModel startProcessModel)
		{
			CamundaAddedVariableData addedVariableData = new();
			addedVariableData.variableName = "changed";
			addedVariableData.variableValue = "string";
			addedVariableData.processInstanceIds = new();
			PairForExecuteService service = new(new CoreContextes.Models.CoreContext());
			PairForExecute pair = new();
			pair.ID_Supervisor = startProcessModel.SupervisorID;
			pair.ID_Deploy = startProcessModel.DeployID;
			pair.ID_Student = startProcessModel.StudentID;
			pair.ID_Organization = startProcessModel.OrganizationID;
			var deployInfo = new DeployService(new CoreContextes.Models.CoreContext()).GetByID(pair.ID_Deploy);
			if (deployInfo is null)
			{
				throw new NotFoundException("Deploy not found");
			}
			var startInfo = new StartAdapter(new Configuration()).Execute(deployInfo.DeployedProcessDefinitionID);
			pair.ProcessID = startInfo.id;
			addedVariableData.processInstanceIds.Add(pair.ProcessID);
			Thread.Sleep(5000);
			new CamundaService(new Configuration()).AddedVariable(addedVariableData).Wait();
			pair.DefinitionID = startInfo.definitionId;
			service.Insert(pair);
			return new { Status = 200, Message = "Process is started" };
		}

		[HttpDelete]
		[ActionName("StopProcess")]
		[Authorize(Roles = "Administrator")]
		public int StopProcess([FromQuery] int id)
		{
			return 1;
		}

		[HttpPost]
		[ActionName("DeployFile")]
		[Authorize(Roles = "Administrator")]
		public object DeployFile([FromForm] DeployFileModel model)
		{
			var user = HttpContext.Items["user"];
			if (user is null)
			{
				throw new NotRoleException();
			}
			Deploy deployInfo = new();
			deployInfo.Name = model.Name;
			deployInfo.ID_Organization = int.Parse(user.ToString());
			try
			{
				using (Stream strm = model.File?.OpenReadStream())
				{
					
					var fileInfo = new DeployAdapter(new Configuration()).Execute(strm, deployInfo);
					deployInfo.ID_Deploy = fileInfo.deploymentId;
					deployInfo.DeployedProcessDefinitionID = fileInfo.id;
					DeployService deployService = new(new CoreContextes.Models.CoreContext());
					deployService.Insert(deployInfo);
				}
			}
			catch (Exception ex)
			{
				return BadRequest(ex);
			}
			return new { Status = 200, Message = "Completed" };
		}

		[HttpGet]
		[ActionName("GetFiles")]
		[Authorize(Roles = "Administrator")]
		public int GetFiles([FromQuery] int id)
		{
			return 1;
		}

		[HttpGet]
		[ActionName("GetInstanses")]
		[Authorize(Roles = "Administrator, Supervisor")]
		public object GetInstanses()
		{
			var role = HttpContext.Items["role"];
			if (role is null)
			{
				throw new NotRoleException();
			}

			var user = HttpContext.Items["user"];
			if (user is null)
			{
				throw new NotRoleException();
			}
			PairForExecuteService pairForExecuteService = new(new CoreContextes.Models.CoreContext());
			List<PairForExecute> pairForExecute = new();


			pairForExecute = role.ToString() switch
			{
				"Administrator" => pairForExecuteService.GetByID_Organization(int.Parse(user.ToString())),
				"Supervisor" => pairForExecuteService.GetByID_Supervisor(int.Parse(user.ToString())),
				_ => throw new NotRoleException("Такой роли не существует"),
			};
			List<GetInstansesModel> items = new();
			StudentService studentService = new(new CoreContextes.Models.CoreContext());
			SupervisorService supervisorService = new(new CoreContextes.Models.CoreContext());
			DeployService deployService = new(new CoreContextes.Models.CoreContext());
			GetInstansesModel tmpModel;
			foreach (var item in pairForExecute)
			{
				tmpModel = new();
				tmpModel.ID = item.ID;
				tmpModel.StudentName = studentService.GetByID(new Student() {ID = item.ID_Student }).FullName;
				tmpModel.SupervisorName = supervisorService.GetByID(new EF.Supervisor() { ID = item.ID_Supervisor }).FullName;
				tmpModel.DeployName = deployService.GetByID(item.ID_Deploy).Name;
				items.Add(tmpModel);
			}
			return JsonSerializer.Serialize(items);
		}

		[HttpGet]
		[ActionName("GetInformationAboutInstanse")]
		[Authorize(Roles = "Administrator, Supervisor, Student")]
		public object GetInformationAboutInstanse([FromQuery] GetInstanceInformationQueryModel model)
		{
			var role = HttpContext.Items["role"];
			if (role is null)
			{
				throw new NotRoleException();
			}

			var user = HttpContext.Items["user"];
			if (user is null)
			{
				throw new NotRoleException();
			}

			PairForExecuteService service = new(new CoreContextes.Models.CoreContext());
			var taskInfo = service.GetByID(model.ID);
			if (taskInfo == null)
			{
				throw new NotFoundException("Заданная задача не найдена");
			}
			GetInstanseInformationModel informationModel = new();
			informationModel.ID = model.ID;
			informationModel.Descriptions = "";

			StoryService storyService = new StoryService(new CoreContextes.Models.CoreContext());
			var storyItems = storyService.GetByID_Process(model.ID);
			foreach (var item in storyItems)
			{
				var tmpItem = new MapperService<Story, StoryInstances>().Execute(item);
				tmpItem.Executor = new RoleService(new CoreContextes.Models.CoreContext()).GetByID(item.ID_Executor).Name;
				informationModel.Stories.Add(tmpItem);
			}

			UserTaskActivityService activityService = new(new CoreContextes.Models.CoreContext());
			var activityInfo = activityService.GetByPairExecute(model.ID);
			if (activityInfo is null)
			{
				informationModel.Descriptions = "Обрабатывается задача. Пожалуйста подождите";
				return JsonSerializer.Serialize(informationModel);
			}
			informationModel.Descriptions = activityInfo.Descriptions;

			UserTaskActivityActionService userTaskActivityActionService = new(new CoreContextes.Models.CoreContext());
			var actions = userTaskActivityActionService.GetByTaskID(activityInfo.ID);
			if (actions is null)
			{
				informationModel.Descriptions = "Обрабатывается задача. Пожалуйста подождите";
				return JsonSerializer.Serialize(informationModel);
			}
			foreach (var act in actions)
			{
				var tmpActions = act.Title.Trim(new Char[] {'[', ']'}).Split(',');
				foreach (var tmpAction in tmpActions)
				{
					informationModel.Actions.Add(tmpAction.Trim(new char[] {'\'', '\"', ' '}));
				}

			}

			UserTaskActivityExecutorsService userTaskActivityExecutorsService = new(new CoreContextes.Models.CoreContext());
			var executorInfo = userTaskActivityExecutorsService.GetByTaskID(activityInfo.ID);
			if (executorInfo is null)
			{
				informationModel.Descriptions = "Обрабатывается задача. Пожалуйста подождите";
				return JsonSerializer.Serialize(informationModel);
			}

			var executors = JsonSerializer.Deserialize<List<Kernel.Entities.Models.Executors>>(executorInfo[0].Type);

			bool isEnable = false;
			foreach (var item in executors)
			{
				if (item.Type == role.ToString())
				{
					isEnable = true;
					break;
				}
			}

			if (isEnable is false)
			{
				informationModel.Actions.Clear();
			}

			switch (role.ToString())
			{
				case "Administrator":
					if (int.Parse(user.ToString()) != taskInfo.ID_Organization)
					{
						informationModel.Actions.Clear(); ;
					}
					break;
				case "Supervisor":
					if (int.Parse(user.ToString()) != taskInfo.ID_Supervisor)
					{
						informationModel.Actions.Clear(); ;
					}
					break;
				case "Student":
					if (int.Parse(user.ToString()) != taskInfo.ID_Student)
					{
						informationModel.Actions.Clear(); ;
					}
					break;
				default:
					informationModel.Actions.Clear();
					break;
			}

			return JsonSerializer.Serialize(informationModel);
		}

		[HttpPost]
		[ActionName("ExecuteTask")]
		[Authorize(Roles = "Administrator, Supervisor, Student")]
		public object ExecuteTask([FromQuery] ExecuteTaskModel taskModel)
		{
			var role = HttpContext.Items["role"];
			if (role is null)
			{
				throw new NotRoleException();
			}

			var user = HttpContext.Items["user"];
			if (user is null)
			{
				throw new NotRoleException();
			}

			PairForExecuteService service = new(new CoreContextes.Models.CoreContext());
			var taskInfo = service.GetByID(taskModel.ID);
			if (taskInfo == null)
			{
				throw new NotFoundException("Заданная задача не найдена");
			}

			UserTaskActivityService activityService = new(new CoreContextes.Models.CoreContext());
			UserTaskActivityExecutorsService executorsService = new(new CoreContextes.Models.CoreContext());
			var activityInfo = activityService.GetByPairExecute(taskModel.ID);
			if (activityInfo is null)
			{
				throw new NotFoundException("Обрабатывается задача. Пожалуйста подождите");
			}

			var executorInfo = executorsService.GetByTaskID(activityInfo.ID);
			if (executorInfo is null)
			{
				throw new NotFoundException("Не задан исполнитель");
			}

			var executors = JsonSerializer.Deserialize<List<Kernel.Entities.Models.Executors>>(executorInfo[0].Type);

			bool isEnable = false;
			foreach (var item in executors)
			{
				if (item.Type == role.ToString())
				{
					isEnable = true;
					break;
				}
			}
			
			if (isEnable is false)
			{
				throw new MemberAccessException("Не имеет доступа");
			}

			switch (role.ToString())
			{
				case "Administrator":
					if (int.Parse(user.ToString()) != taskInfo.ID_Organization)
					{
						throw new MemberAccessException("Не имеет доступа");
					}
					break;
				case "Supervisor":
					if (int.Parse(user.ToString()) != taskInfo.ID_Supervisor)
					{
						throw new MemberAccessException("Не имеет доступа");
					}
					break;
				case "Student":
					if (int.Parse(user.ToString()) != taskInfo.ID_Student)
					{
						throw new MemberAccessException("Не имеет доступа");
					}
					break;
				default:
					throw new NotRoleException("Такой роли не существует");
					break;
			}
			
			UserTaskInput input = new();
			input.Action = taskModel.Action;
			input.ID_Pair = taskModel.ID;
			input.ProcessInstanceID = taskInfo.ProcessID;
			UserTaskManager manager = new(new Configuration());
			var story = manager.Complete(input, activityInfo);
			StoryService storyService = new(new CoreContextes.Models.CoreContext());
			story.DateTime = new DateTimeWithZone(DateTime.Now, TimeZoneInfo.Utc).UniversalTime;
			story.ID_Executor = new RoleService(new CoreContextes.Models.CoreContext()).GetByName(role.ToString()).ID;
			story.ID_Process = taskModel.ID;
			story.Action = taskModel.Action;
			storyService.Insert(story);

			return new {Status = 200, Message = "Completed" };
		}
	}
}
