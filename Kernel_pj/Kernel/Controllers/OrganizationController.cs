﻿using AutoMapper;
using Kernel.Entities.Exceptions;
using Kernel.Entities.Services;
using Kernel.Models;
using Kernel.UseCases.Handler;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Kernel.Controllers.Organization
{
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class OrganizationController : ControllerBase
	{
		private readonly OrganizationHandler handler = new();

		[HttpPost]
		[ActionName("CreateOrganization")]
		public object CreateOrganization([FromForm] AddedOrganizationModel model)
		{
			var item = new MapperService<AddedOrganizationModel, EF.Organization>().Execute(model);
			handler.Add(item);
			return new {Status = 200, Message = "Organization create successfull" };
		}

		[HttpGet]
		[ActionName("GetInformation")]
		[Authorize(Roles = "Administrator, Supervisor, Student, Organization")]
		public object Get([FromQuery] GetOrganizationInfo model)
		{
			return handler.Get(new MapperService<GetOrganizationInfo, EF.Organization>().Execute(model));
		}

		[HttpGet]
		[ActionName("GetBranches")]
		[Authorize(Roles = "Administrator, Supervisor, Student")]
		public object GetBranches([FromQuery] GetOrganizationInfo model)
		{
			return handler.GetBranches(new MapperService<GetOrganizationInfo, EF.Organization>().Execute(model));
		}

		[HttpPost]
		[ActionName("CreateBranch")]
		[Authorize(Roles = "Administrator")]
		public object CreateBranch([FromForm] AddedBranchModel model)
		{
			var item = new MapperService<AddedBranchModel, EF.Organization>().Execute(model);
			if (HttpContext.Items["user"] is null)
			{
				throw new NotUserException();
			}
			item.ID_Parent = int.Parse(HttpContext.Items["user"].ToString());
			handler.Add(item);
			return new { Status = 200, Message = "Branch create successfull" };
		}

		[HttpDelete]
		[ActionName("DeleteInformation")]
		[Authorize(Roles = "Administrator")]
		public object Delete([FromQuery] GetOrganizationInfo model)
		{
			handler.Delete(new MapperService<GetOrganizationInfo, EF.Organization>().Execute(model));
			return new { Status = 200, Message = "Information delete successfull" };
		}

		[HttpPost]
		[ActionName("ModificateInformation")]
		[Authorize(Roles = "Administrator")]
		public object Modificate([FromForm] AddedBranchModel model)
		{
			handler.Update(new MapperService<AddedBranchModel, EF.Organization>().Execute(model));
			return new { Status = 200, Message = "Information update successfull" };
		}
	}
}
