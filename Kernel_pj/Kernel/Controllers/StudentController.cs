﻿using Kernel.Entities.Services;
using Kernel.Models;
using Kernel.UseCases.Handler;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Kernel.Controllers.Organization
{
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class StudentController : ControllerBase
	{
		private readonly StudentHandler handler = new();
		[HttpGet]
		[ActionName("GetInformation")]
		[Authorize(Roles = "Administrator, Supervisor, Student")]
		public object Get([FromQuery] GetStudentInfo model)
		{
			return handler.Get(new MapperService<GetStudentInfo, EF.Student>().Execute(model));
		}

		[HttpPost]
		[ActionName("Create")]
		[Authorize(Roles = "Administrator")]
		public object Create([FromForm] AddedStudentModel model)
		{
			var item = new MapperService<AddedStudentModel, EF.Student>().Execute(model);
			handler.Add(item);
			return new { Status = 200, Message = "Student create successfull" };
		}


		[HttpDelete]
		[ActionName("DeleteInformation")]
		[Authorize(Roles = "Administrator")]
		public object Delete([FromQuery] GetStudentInfo model)
		{
			handler.Delete(new MapperService<GetStudentInfo, EF.Student>().Execute(model));
			return new { Status = 200, Message = "Information delete successfull" };
		}

		[HttpPost]
		[ActionName("ModificateInformation")]
		[Authorize(Roles = "Administrator")]
		public object Modificate([FromForm] AddedStudentModel model)
		{
			handler.Update(new MapperService<AddedStudentModel, EF.Student>().Execute(model));
			return new { Status = 200, Message = "Information update successfull" };
		}

	}
}
