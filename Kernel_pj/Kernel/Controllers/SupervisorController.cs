﻿using Kernel.Entities.Services;
using Kernel.Models;
using Kernel.UseCases.Handler;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Kernel.Controllers.Supervisor
{
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class SupervisorController : ControllerBase
	{
		private readonly SupervisorHandler handler = new();
		[HttpGet]
		[ActionName("GetInformation")]
		[Authorize(Roles = "Administrator, Supervisor, Student")]
		public object Get([FromQuery] GetSupervisorInfo model)
		{
			return handler.Get(new MapperService<GetSupervisorInfo, EF.Supervisor>().Execute(model));
		}

		[HttpPost]
		[ActionName("Create")]
		[Authorize(Roles = "Administrator")]
		public object Create([FromForm] AddedSupervisorModel model)
		{
			var item = new MapperService<AddedSupervisorModel, EF.Supervisor>().Execute(model);
			handler.Add(item);
			return new { Status = 200, Message = "Supervisor create successfull" };
		}


		[HttpDelete]
		[ActionName("DeleteInformation")]
		[Authorize(Roles = "Administrator")]
		public object Delete([FromQuery] GetSupervisorInfo model)
		{
			handler.Delete(new MapperService<GetSupervisorInfo, EF.Supervisor>().Execute(model));
			return new { Status = 200, Message = "Information delete successfull" };
		}

		[HttpPost]
		[ActionName("ModificateInformation")]
		[Authorize(Roles = "Administrator")]
		public object Modificate([FromForm] AddedSupervisorModel model)
		{
			handler.Update(new MapperService<AddedSupervisorModel, EF.Supervisor>().Execute(model));
			return new { Status = 200, Message = "Information update successfull" };
		}
	}
}
