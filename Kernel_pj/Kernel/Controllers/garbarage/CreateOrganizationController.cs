﻿using AutoMapper;
using EF.Services.Implementation;
using Kernel.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Kernel.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class CreateOrganizationController : ControllerBase
	{
		private readonly ILogger<CreateOrganizationController> _logger;
		//private readonly OrganizationUse useCase = new OrganizationUse();
		private readonly OrganizationService service = new(new CoreContextes.Models.CoreContext());

		public CreateOrganizationController(ILogger<CreateOrganizationController> logger)
		{
			_logger = logger;
		}

		[HttpPost(Name = "CreateOrganization")]
		[Authorize(Roles = "Administrator,  ss")]
		public object Create([FromForm] AddedOrganizationModel organization)
		{
			var config = new MapperConfiguration(cfg => cfg.CreateMap<AddedOrganizationModel, EF.Organization>());
			var mapper = new Mapper(config);
			// Выполняем сопоставление
			var item = mapper.Map<AddedOrganizationModel, EF.Organization>(organization);
			
			service.Insert(item);

			Console.WriteLine(this.HttpContext.Items["role"]);
			return new { Value = "ok", Name  = organization.Name};
		}
	}
}
