﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Kernel.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ToAuthorizateController : ControllerBase
	{
		private readonly ILogger<ToAuthorizateController> _logger;

		public ToAuthorizateController(ILogger<ToAuthorizateController> logger)
		{
			_logger = logger;
		}

		[HttpGet(Name = "ToAuthorizate")]
		//[Authorize]
		public IEnumerable<WeatherForecast> Get()
		{
			return null;
		}
	}
}
