﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Kernel.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UnAuthorizateController : ControllerBase
	{
		private readonly ILogger<UnAuthorizateController> _logger;

		public UnAuthorizateController(ILogger<UnAuthorizateController> logger)
		{
			_logger = logger;
		}

		[HttpGet(Name = "UnAuthorizate")]
		//[Authorize]
		public IEnumerable<WeatherForecast> Get()
		{
			return null;
		}
	}
}
