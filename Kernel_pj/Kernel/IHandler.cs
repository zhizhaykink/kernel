﻿namespace Kernel
{
	public interface IHandler
	{
		public void Exec();
	}
	public class Handler1 : IHandler
	{
		public void Exec()
		{
			Console.WriteLine("IT IS HANDLER1");
		}
	}

	public class Service1 : BackgroundService
	{
		public Service1()
		{
			//_handler = handler; 
		}
		public Service1(IHandler handler)
		{
			_handler = handler; 
		}
		IHandler _handler;
		public void Execute()
		{
			_handler.Exec();
		}

		protected override Task ExecuteAsync(CancellationToken stoppingToken)
		{
			_handler.Exec();
			return null;
		}
	}
}
