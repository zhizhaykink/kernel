﻿namespace Kernel
{
	public class RESTMiddleware
	{
        private readonly RequestDelegate _next;
        private readonly ILogger<RESTMiddleware> _logger;
        public RESTMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory?.CreateLogger<RESTMiddleware>() ??
            throw new ArgumentNullException(nameof(loggerFactory));
        }
        public async Task InvokeAsync(HttpContext context)
        {
            //_logger.LogInformation($"Request URL: {Microsoft.AspNetCore.Http.Extensions.UriHelper.GetDisplayUrl(context.Request)}");
            _logger.LogInformation($"Methods {Microsoft.AspNetCore.Http.Extensions.UriHelper.GetDisplayUrl(context.Request)}");
            //_logger.LogInformation($"query {context.Request.Query}");
            //_logger.LogInformation($"queryString {context.Request.QueryString}");
            //_logger.LogInformation($"method {context.Request.Method}");
            //_logger.LogInformation($"route {context.Request.RouteValues}");
            await context.Response.WriteAsync($"Hello world! {Microsoft.AspNetCore.Http.Extensions.UriHelper.GetDisplayUrl(context.Request)}");
            //await this._next(context);
        }
    }

    public static class RESTMiddlewareExtensions  
{  
    public static IApplicationBuilder UseLogUrl(this IApplicationBuilder app)  
    {  
        return app.UseMiddleware<RESTMiddleware>();  
    }  
}
}

