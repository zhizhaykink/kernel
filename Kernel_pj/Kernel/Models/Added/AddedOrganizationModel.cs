﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kernel.Models
{
    public class AddedOrganizationModel
    {
		public string Name { get; set; }
		public string Director { get; set; }
		public string Email { get; set; }
		public string Login { get; set; }
		public string Pass { get; set; }
	}
}
