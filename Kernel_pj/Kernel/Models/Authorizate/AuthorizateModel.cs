﻿namespace Kernel.Models.Authorizate
{
	public class AuthorizateModel
	{
		public string Login { get; set; }
		public string Pass { get; set; }
		public string Role	{ get; set; }
	}
}
