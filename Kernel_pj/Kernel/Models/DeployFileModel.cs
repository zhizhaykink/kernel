﻿namespace Kernel.Models
{
	public class DeployFileModel
	{
		public IFormFile File { get; set; }
		public string Name { get; set; }
	}
}
