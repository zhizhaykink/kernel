﻿namespace Kernel.Models
{
	public class ExecuteTaskModel
	{
		public int ID { get; set; }
		public string Action { get; set; }
	}
}
