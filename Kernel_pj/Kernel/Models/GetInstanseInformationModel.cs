﻿using EF;

namespace Kernel.Models
{
	public class GetInstanseInformationModel
	{
		public int ID { get; set; }
		public List<StoryInstances> Stories { get; set; } = new List<StoryInstances>();
		public List<string> Actions { get; set; } = new List<string>();
		public string? Descriptions { get; set; }
	}
}
