﻿namespace Kernel.Models
{
	public class GetInstansesModel
	{
		public int ID { get; set; }
		public string StudentName { get; set; }
		public string SupervisorName { get; set; }
		public string DeployName { get; set; }
	}
}
