﻿namespace Kernel.Models
{
	public class StartProcessModel
	{
		public int SupervisorID { get; set; }
		public int StudentID { get; set; }
		public int DeployID { get; set; }
		public int OrganizationID { get; set; }
	}
}
