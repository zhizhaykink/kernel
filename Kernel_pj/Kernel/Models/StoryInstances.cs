﻿namespace Kernel.Models
{
	public class StoryInstances
	{
		public int ID { get; set; }
		public int ID_Process { get; set; }
		public string Executor { get; set; }
		public string? Topic { get; set; }
		public string? ActivityInstancedID { get; set; }
		public string? StoryID { get; set; }
		public DateTime DateTime { get; set; }
		public string? Action { get; set; }
		public string? ProcessDefinitionID { get; set; }
		public string? WorkerID { get; set; }

	}
}
