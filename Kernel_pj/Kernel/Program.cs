
using Kernel;
using Microsoft.Extensions.DependencyInjection;
using System.Text.Json;
using TestOfCamundaClass;
using TestOfCamundaClass.Handler;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var confiuration = new Configuration();
var jsonString = File.ReadAllText("Configuration.json");
var element = JsonSerializer.Deserialize<Configuration>(jsonString);
confiuration.GetConfiguration = element;
UserTaskActivityWorker userTaskWorker = new(confiuration);
new Thread(() => userTaskWorker.Run()).Start();
//userTaskWorker.Run();
//builder.Host.ConfigureServices((_, service) => service.AddHostedService<UserTaskActivityWorker>().ConfigureOptions(confiuration));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
	app.UseSwagger();
	app.UseSwaggerUI();
}


app.UseHttpsRedirection();
//app.UseAuthentication();
//app.UseAuthorization();


app.MapControllers();

app.Run();
