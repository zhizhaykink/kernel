﻿using EF;
using Microsoft.EntityFrameworkCore;
using Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreContextes.Models
{
	public class CoreContext : DbContext
	{
        public DbSet<Token> Token { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Deploy> Deploy { get; set; }
        public DbSet<Organization> Organization { get; set; }
        public DbSet<OrganizationToken> OrganizationToken { get; set; }
        public DbSet<PairForExecute> PairForExecute { get; set; }
        public DbSet<Story> Story { get; set; }
        public DbSet<Student> Student { get; set; }
        public DbSet<StudentToken> StudentToken { get; set; }
        public DbSet<Supervisor> Supervisor { get; set; }
        public DbSet<SupervisorToken> SupervisorToken { get; set; }
        public DbSet<UserTaskActivity> UserTaskActivity { get; set; }
        public DbSet<UserTaskActivityAction> UserTaskActivityAction { get; set; }
        public DbSet<UserTaskActivityExecutors> UserTaskActivityExecutors { get; set; }
        public DbSet<StatusTask> StatusTask { get; set; }


        public CoreContext()
        {
          //  Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql($"Host=localhost;Port=5432;Database=TEST2;Username=postgres;Password=uhtykfylbz");
        }
    }
}
