﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF
{
	public class OrganizationToken
	{
		[Key]
		public int ID { get; set; }
		[ForeignKey("Organization")]
		public int ID_Organization { get; set; }
		public string GUID { get; set; }
		public DateTime? Live { get; set; }
	}
}
