﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF
{
	public class PairForExecute
	{
		[Key]
		public int ID { get; set; }

		[ForeignKey("Student")]
		public int ID_Student { get; set; }

		[ForeignKey("Supervisor")]
		public int ID_Supervisor { get; set; }
		[ForeignKey("Organization")]
		public int ID_Organization { get; set; }

		[ForeignKey("Deploy")]
		public int ID_Deploy { get; set; }

		public string? ProcessID {  get; set; }
		public string? DefinitionID { get; set; }
		public int? Grade { get; set; }
		public string? TopicName { get; set; }
		public DateTime? DateTime { get; set; }
	}
}
