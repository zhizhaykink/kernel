﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF
{
	public class Story
	{
		[Key]
		public int ID { get; set; }
		[ForeignKey("PairForExecute")]
		public int ID_Process { get; set; }
		[ForeignKey("Role")]
		public int ID_Executor { get; set; }
		public string? Topic { get; set; }
		public int? Counter { get; set; }
		public string? ExecutionID { get; set; }
		public string? ProcessDefinitionID { get; set; }
		public string? WorkerID { get; set; }
		public string? ActivityInstancedID { get; set; }
		public string? StoryID { get; set; }
		public DateTime DateTime { get; set; }
		public string? Action { get;set; }
	}
}
