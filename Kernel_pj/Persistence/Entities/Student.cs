﻿using Persistence.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF
{
	public class Student : IActor
	{
		[Key]
		public int ID { get; set; }
		[ForeignKey("Organization")]
		public int? ID_Organization { get; set; }
		public string? FullName { get; set; }
		public string Login { get; set; }
		public string Pass { get; set; }
		public string? Email { get; set; }

	}
}
