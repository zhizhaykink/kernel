﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF
{
	public class SupervisorToken
	{
		[Key]
		public int ID { get; set; }
		[ForeignKey("Supervisor")]
		public int? ID_Supervisor { get; set; }
		public string GUID { get; set; }
		public DateTime? Live { get; set; }
	}
}
