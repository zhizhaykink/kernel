﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF
{
	public class Token
	{
		[Key]
		public int ID { get; set; }
		public int? ID_User { get; set; }
		public int? ID_Role { get; set; }
		public string GUID { get; set; }
		public TimeSpan? Live { get; set; }
	}
}
