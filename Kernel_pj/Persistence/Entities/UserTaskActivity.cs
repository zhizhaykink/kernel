﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Entities
{
	public class UserTaskActivity
	{
		[Key]
		public int ID { get; set; }
		public int Status { get; set; } 
		public int PairForExecuteID { get; set; }
		public string? ExernalTaskID { get; set; }
		public string? Descriptions { get; set; }
	}
}
