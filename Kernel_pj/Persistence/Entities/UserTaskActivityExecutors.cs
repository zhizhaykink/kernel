﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Entities
{
	public class UserTaskActivityExecutors
	{
		[Key]
		public int ID { get; set; }
		public int UserTaskActivityID { get; set; }
		public string Type { get; set; }
	}
}
