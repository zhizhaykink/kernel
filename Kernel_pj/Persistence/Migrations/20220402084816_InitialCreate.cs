﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace Persistence.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
               name: "Organization",
               columns: table => new
               {
                   ID = table.Column<int>(type: "integer", nullable: false)
                       .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                   Name = table.Column<string>(type: "text", nullable: true),
                   ID_Parent = table.Column<int>(type: "integer", nullable: true),
                   Director = table.Column<string>(type: "text", nullable: true),
                   Email = table.Column<string>(type: "text", nullable: true),
                   Login = table.Column<string>(type: "text", nullable: false),
                   Pass = table.Column<string>(type: "text", nullable: false)
               },
               constraints: table =>
               {
                   table.PrimaryKey("PK_Organization", x => x.ID);
                   table.ForeignKey(
                     name: "FK_Parent_Organization_Organization",
                     column: x => x.ID_Parent,
                     principalTable: "Organization",
                     principalColumn: "ID",
                     onDelete: ReferentialAction.Cascade);
               });



            migrationBuilder.CreateTable(
               name: "Student",
               columns: table => new
               {
                   ID = table.Column<int>(type: "integer", nullable: false)
                       .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                   ID_Organization = table.Column<int>(type: "integer", nullable: true),
                   FullName = table.Column<string>(type: "text", nullable: true),
                   Login = table.Column<string>(type: "text", nullable: false),
                   Pass = table.Column<string>(type: "text", nullable: false),
                   Email = table.Column<string>(type: "text", nullable: true)
               },
               constraints: table =>
               {
                   table.PrimaryKey("PK_Student", x => x.ID);
                   table.ForeignKey(
                    name: "FK_Student_organization",
                    column: x => x.ID_Organization,
                    principalTable: "Organization",
                    principalColumn: "ID",
                    onDelete: ReferentialAction.Cascade);
               });

            migrationBuilder.CreateTable(
                name: "StudentToken",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ID_Student = table.Column<int>(type: "integer", nullable: false),
                    GUID = table.Column<string>(type: "text", nullable: false),
                    Live = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentToken", x => x.ID);
                    table.ForeignKey(
                    name: "FK_TokenStudent",
                    column: x => x.ID_Student,
                    principalTable: "Student",
                    principalColumn: "ID",
                    onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Supervisor",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ID_Organization = table.Column<int>(type: "integer", nullable: true),
                    FullName = table.Column<string>(type: "text", nullable: true),
                    Login = table.Column<string>(type: "text", nullable: false),
                    Pass = table.Column<string>(type: "text", nullable: false),
                    Email = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Supervisor", x => x.ID);
                    table.ForeignKey(
                     name: "FK_Supervisor_organization",
                     column: x => x.ID_Organization,
                     principalTable: "Organization",
                     principalColumn: "ID",
                     onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SupervisorToken",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ID_Supervisor = table.Column<int>(type: "integer", nullable: true),
                    GUID = table.Column<string>(type: "text", nullable: false),
                    Live = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupervisorToken", x => x.ID);
                    table.ForeignKey(
                     name: "FK_SupervisorToken",
                     column: x => x.ID_Supervisor,
                     principalTable: "Supervisor",
                     principalColumn: "ID",
                     onDelete: ReferentialAction.Cascade);
                });



            migrationBuilder.CreateTable(
                name: "Deploy",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ID_Organization = table.Column<int>(type: "integer", nullable: false),
                    ID_Deploy = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Source = table.Column<string>(type: "text", nullable: true),
                    TenantID = table.Column<string>(type: "text", nullable: true),
                    DeployedProcessDefinitionID = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Deploy", x => x.ID);
                    table.ForeignKey(
                       name: "FK_Deploy_Organization_Organization",
                       column: x => x.ID_Organization,
                       principalTable: "Organization",
                       principalColumn: "ID",
                       onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Mode",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mode", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "OrganizationToken",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ID_Organization = table.Column<int>(type: "integer", nullable: false),
                    GUID = table.Column<string>(type: "text", nullable: false),
                    Live = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrganizationToken", x => x.ID);
                    table.ForeignKey(
                     name: "FK_Token_Organization_Organization",
                     column: x => x.ID_Organization,
                     principalTable: "Organization",
                     principalColumn: "ID",
                     onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PairForExecute",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ID_Student = table.Column<int>(type: "integer", nullable: false),
                    ID_Supervisor = table.Column<int>(type: "integer", nullable: false),
                    ID_Deploy = table.Column<int>(type: "integer", nullable: false),
                    ProcessID = table.Column<string>(type: "text", nullable: true),
                    DefinitionID = table.Column<string>(type: "text", nullable: true),
                    Grade = table.Column<int>(type: "integer", nullable: true),
                    TopicName = table.Column<string>(type: "text", nullable: true),
                    DateTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PairForExecute", x => x.ID);
                    table.ForeignKey(
                     name: "FK_St_pair",
                     column: x => x.ID_Student,
                     principalTable: "Student",
                     principalColumn: "ID",
                     onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                     name: "FK_Supervisor_pair",
                     column: x => x.ID_Supervisor,
                     principalTable: "Supervisor",
                     principalColumn: "ID",
                     onDelete: ReferentialAction.Cascade); 
                    table.ForeignKey(
                      name: "FK_ID_Deploy",
                      column: x => x.ID_Deploy,
                      principalTable: "Deploy",
                      principalColumn: "ID",
                      onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Story",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ID_Process = table.Column<int>(type: "integer", nullable: false),
                    ID_Mode = table.Column<int>(type: "integer", nullable: false),
                    Topic = table.Column<string>(type: "text", nullable: true),
                    Counter = table.Column<int>(type: "integer", nullable: true),
                    ExecutionID = table.Column<string>(type: "text", nullable: true),
                    ProcessDefinitionID = table.Column<string>(type: "text", nullable: true),
                    WorkerID = table.Column<string>(type: "text", nullable: true),
                    ActivityInstancedID = table.Column<string>(type: "text", nullable: true),
                    StoryID = table.Column<string>(type: "text", nullable: true),
                    DateTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Story", x => x.ID);
                    table.ForeignKey(
                     name: "FK_ID_Process",
                     column: x => x.ID_Process,
                     principalTable: "PairForExecute",
                     principalColumn: "ID",
                     onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                     name: "FK_StoryMode",
                     column: x => x.ID_Mode,
                     principalTable: "Mode",
                     principalColumn: "ID",
                     onDelete: ReferentialAction.Cascade);
                });

           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Deploy");

            migrationBuilder.DropTable(
                name: "Mode");

            migrationBuilder.DropTable(
                name: "Organization");

            migrationBuilder.DropTable(
                name: "OrganizationToken");

            migrationBuilder.DropTable(
                name: "PairForExecute");

            migrationBuilder.DropTable(
                name: "Story");

            migrationBuilder.DropTable(
                name: "Student");

            migrationBuilder.DropTable(
                name: "StudentToken");

            migrationBuilder.DropTable(
                name: "Supervisor");

            migrationBuilder.DropTable(
                name: "SupervisorToken");
        }
    }
}
