﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace Persistence.Migrations
{
    public partial class fixUserTaskActivity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StatusTask",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Title = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatusTask", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "UserTaskActivity",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    PairForExecuteID = table.Column<int>(type: "integer", nullable: false),
                    ExernalTaskID = table.Column<string>(type: "text", nullable: true),
                    Descriptions = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTaskActivity", x => x.ID);

                    table.ForeignKey(
                    name: "FK_ID_PairForExecution",
                    column: x => x.PairForExecuteID,
                    principalTable: "PairForExecute",
                    principalColumn: "ID",
                    onDelete: ReferentialAction.Cascade);

                    table.ForeignKey(
                    name: "FK_ID_Status",
                    column: x => x.Status,
                    principalTable: "StatusTask",
                    principalColumn: "ID",
                    onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserTaskActivityAction",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserTaskActivityID = table.Column<int>(type: "integer", nullable: false),
                    Title = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTaskActivityAction", x => x.ID);

                    table.ForeignKey(
                    name: "FK_ID_UserTaskActivity",
                    column: x => x.UserTaskActivityID,
                    principalTable: "UserTaskActivity",
                    principalColumn: "ID",
                    onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserTaskActivityExecutors",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserTaskActivityID = table.Column<int>(type: "integer", nullable: false),
                    Type = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTaskActivityExecutors", x => x.ID);

                    table.ForeignKey(
                   name: "FK_ID_UserTaskActivity",
                   column: x => x.UserTaskActivityID,
                   principalTable: "UserTaskActivity",
                   principalColumn: "ID",
                   onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StatusTask");

            migrationBuilder.DropTable(
                name: "UserTaskActivity");

            migrationBuilder.DropTable(
                name: "UserTaskActivityAction");

            migrationBuilder.DropTable(
                name: "UserTaskActivityExecutors");
        }
    }
}
