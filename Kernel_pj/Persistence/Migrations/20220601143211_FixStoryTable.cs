﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Persistence.Migrations
{
    public partial class FixStoryTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ID_Mode",
                table: "Story",
                newName: "ID_Executor");

            migrationBuilder.AddForeignKey(
                name : "ID_Executor_FK",
                table : "Story",
                column: "ID_Executor",
                principalTable: "Role",
                principalColumn: "ID");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateTime",
                table: "Story",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ID_Executor",
                table: "Story",
                newName: "ID_Mode");

            migrationBuilder.DropForeignKey(
                name: "ID_Executor_FK",
                table: "Story"
                );

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateTime",
                table: "Story",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");
        }
    }
}
