﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Persistence.Migrations
{
    public partial class Added_ID_Organization_INTO_Pair_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ID_Organization",
                table: "PairForExecute",
                type: "integer",
                nullable: false,
                defaultValue: 0);
            migrationBuilder.AddForeignKey(
               name: "ID_Organization_FK",
               table: "PairForExecute",
               column: "ID_Organization",
               principalTable: "Organization",
               principalColumn: "ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ID_Organization",
                table: "PairForExecute");
            migrationBuilder.DropForeignKey(
                name: "ID_Organization_FK",
                table: "PairForExecute");
        }
    }
}
