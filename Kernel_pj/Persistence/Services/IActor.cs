﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Services
{
	public interface IActor
	{
		public int ID { get; set; }
		public string Login { get; set; }
		public string Pass { get; set; }
		public string? Email { get; set; }
	}
}
