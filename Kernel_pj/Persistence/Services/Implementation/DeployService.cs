﻿using CoreContextes.Models;

namespace EF.Services.Implementation
{
	public class DeployService
	{
		private CoreContext _coreContext;
		public DeployService(CoreContext coreContext)
		{
			_coreContext = coreContext;
		}

		public Deploy GetByID(int id)
		{
			return _coreContext.Deploy
				.Where(p => p.ID == id)
				.FirstOrDefault();
		}

		public List<Deploy> GetByID_Organization(int id)
		{
			return _coreContext.Deploy
				.Where(p => p.ID_Organization == id).ToList();
		}

		public void Insert(Deploy deploy)
		{
			_coreContext.Deploy.Add(deploy);
			_coreContext.SaveChanges();
		}
		public void Delete(int id)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			_coreContext.Deploy.Remove(item);
			_coreContext.SaveChanges();
		}
	}
}
