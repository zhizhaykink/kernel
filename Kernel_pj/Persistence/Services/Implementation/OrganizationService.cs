﻿using CoreContextes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Services.Implementation
{
	public class OrganizationService
	{
		private CoreContext _coreContext;
		public OrganizationService(CoreContext coreContext)
		{
			_coreContext = coreContext;
		}
		public Organization GetByID(int id)
		{
			return _coreContext.Organization
				.Where(p => p.ID == id)
				.FirstOrDefault();
		}

		public Organization GetByLogin(string login)
		{
			return _coreContext.Organization
				.Where(p => p.Login == login)
				.FirstOrDefault();
		}

		public Organization GetByName(string name)
		{
			return _coreContext.Organization
				.Where(p => p.Login == name)
				.FirstOrDefault();
		}

		public void Insert(Organization organization)
		{
			_coreContext.Organization.Add(organization);
			_coreContext.SaveChanges();
		}

		public void UpdatePass(int id, string pass)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			item.Pass = pass;
			_coreContext.SaveChanges();
		}
		public void UpdateEmail(int id, string email)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			item.Email = email;
			_coreContext.SaveChanges();
		}
		public void UpdateLogin(int id, string login)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			item.Login = login;
			_coreContext.SaveChanges();
		}

		public void UpdateName(int id, string name)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			item.Name = name;
			_coreContext.SaveChanges();
		}

		public void UpdateDirector(int id, string director)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			item.Director = director;
			_coreContext.SaveChanges();
		}

		public void Delete(int id)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			_coreContext.Organization.Remove(item);
			_coreContext.SaveChanges();
		}
	}
}
