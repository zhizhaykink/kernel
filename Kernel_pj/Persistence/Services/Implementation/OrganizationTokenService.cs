﻿using CoreContextes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Services.Implementation
{
	public class OrganizationTokenService
	{
		private CoreContext _coreContext;
		public OrganizationTokenService(CoreContext coreContext)
		{
			_coreContext = coreContext;
		}
		public OrganizationToken GetByID(int id)
		{
			return _coreContext.OrganizationToken
				.Where(p => p.ID == id)
				.FirstOrDefault();
		}

		public List<OrganizationToken> GetByID_Organization(int id)
		{
			return _coreContext.OrganizationToken
				.Where(p => p.ID_Organization == id)
				.ToList();
		}

		public OrganizationToken GetByGUID(string guid)
		{
			return _coreContext.OrganizationToken
				.Where(p => p.GUID == guid)
				.FirstOrDefault();
		}

		public void Insert(OrganizationToken token)
		{
			_coreContext.OrganizationToken.Add(token);
			_coreContext.SaveChanges();
		}
		public void Delete(int id)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			_coreContext.OrganizationToken.Remove(item);
			_coreContext.SaveChanges();
		}
		public DateTime? CheckLive (int id)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return null;
			}
			return item.Live;
		}

		public DateTime? CheckLive(string GUID)
		{
			var item = GetByGUID(GUID);
			if (item is null)
			{
				return null;
			}
			return item.Live;
		}
	}
}
