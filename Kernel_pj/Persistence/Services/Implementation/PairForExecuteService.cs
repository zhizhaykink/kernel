﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreContextes.Models;
namespace EF.Services.Implementation
{
	public class PairForExecuteService
	{
		private CoreContext _coreContext;
		public PairForExecuteService(CoreContext coreContext)
		{
			_coreContext = coreContext;
		}
		public PairForExecute GetByID(int id)
		{
			return _coreContext.PairForExecute
				.Where(p => p.ID == id)
				.FirstOrDefault();
		}

		public List<PairForExecute> GetByID_Supervisor(int id)
		{
			return _coreContext.PairForExecute
				.Where(p => p.ID_Supervisor == id)
				.ToList();
		}

		public List<PairForExecute> GetByID_Organization(int id)
		{
			return _coreContext.PairForExecute
				.Where(p => p.ID_Organization == id)
				.ToList();
		}
		public List<PairForExecute> GetByID_Student(int id)
		{
			return _coreContext.PairForExecute
				.Where(p => p.ID_Student == id)
				.ToList();
		}
		public List<PairForExecute> GetByID_Deploy(int id)
		{
			return _coreContext.PairForExecute
				.Where(p => p.ID_Deploy == id)
				.ToList();
		}

		public List<PairForExecute> GetByID_ProcessID(string id)
		{
			return _coreContext.PairForExecute
				.Where(p => p.ProcessID == id)
				.ToList();
		}

		public void Insert(PairForExecute execute)
		{
			_coreContext.PairForExecute.Add(execute);
			_coreContext.SaveChanges();
		}

		public void UpdateGrade(int id, int grade)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			item.Grade = grade;
			_coreContext.SaveChanges();
		}

		public void UpdateTopicName(int id, string topicName)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			item.TopicName = topicName;
			_coreContext.SaveChanges();
		}

		public void Delete(int id)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			_coreContext.PairForExecute.Remove(item);
			_coreContext.SaveChanges();
		}
	}
}
