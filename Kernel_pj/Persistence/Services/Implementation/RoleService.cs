﻿using CoreContextes.Models;
using EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Services.Implementation
{
	public class RoleService
	{
		private CoreContext _coreContext;
		public RoleService(CoreContext coreContext)
		{
			_coreContext = coreContext;
		}
		public Role GetByID(int id)
		{
			return _coreContext.Role
				.Where(p => p.ID == id)
				.FirstOrDefault();
		}

		public Role GetByName(string name)
		{
			return _coreContext.Role
				.Where(p => p.Name == name)
				.FirstOrDefault();
		}
	}
}
