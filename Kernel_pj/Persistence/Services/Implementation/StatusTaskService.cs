﻿using CoreContextes.Models;
using EF;
using Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Services.Implementation
{
	public class StatusTaskService
	{
		private CoreContext _coreContext;
		public StatusTaskService(CoreContext coreContext)
		{
			_coreContext = coreContext;
		}

		public StatusTask GetByID(int id)
		{
			return _coreContext.StatusTask
				.Where(p => p.ID == id)
				.FirstOrDefault();
		}

		public void Insert(StatusTask statusTask)
		{
			_coreContext.StatusTask.Add(statusTask);
			_coreContext.SaveChanges();
		}
		public void Delete(int id)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			_coreContext.StatusTask.Remove(item);
			_coreContext.SaveChanges();
		}

		public void UpdateExternalTask(int id, string title)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			item.Title = title;
			_coreContext.SaveChanges();
		}
	}
}
