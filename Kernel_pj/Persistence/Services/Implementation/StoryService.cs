﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreContextes.Models;
namespace EF.Services.Implementation
{
	public class StoryService
	{
		private CoreContext _coreContext;
		public StoryService(CoreContext coreContext)
		{
			_coreContext = coreContext;
		}
		public Story GetByID(int id)
		{
			return _coreContext.Story
				.Where(p => p.ID == id)
				.FirstOrDefault();
		}

		public List<Story> GetByID_Process(int id)
		{
			return _coreContext.Story
				.Where(p => p.ID_Process == id)
				.ToList();
		}

		public void Insert(Story story)
		{
			_coreContext.Story.Add(story);
			_coreContext.SaveChanges();
		}

		public void UpdateCounter(int id, int counter)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			item.Counter = counter;
			_coreContext.SaveChanges();
		}

		public void DeleteByID(int id)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			_coreContext.Story.Remove(item);
			_coreContext.SaveChanges();
		}

		public void DeleteByID_Process(int id)
		{
			var items = GetByID_Process(id);
			if (items is null)
			{
				return;
			}
			foreach (var item in items)
			{
				_coreContext.Story.Remove(item);
			}
			_coreContext.SaveChanges();
		}
	}
}
