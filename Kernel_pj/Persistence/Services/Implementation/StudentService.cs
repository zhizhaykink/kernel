﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreContextes.Models;
namespace EF.Services.Implementation
{
	public class StudentService
	{
		private CoreContext _coreContext;
		public StudentService(CoreContext coreContext)
		{
			_coreContext = coreContext;
		}
		public Student GetByID(Student student)
		{
			return _coreContext.Student
				.Where(p => p.ID == student.ID)
				.FirstOrDefault();
		}

		public Student GetByLogin(Student student)
		{
			return _coreContext.Student
				.Where(p => p.Login == student.Login)
				.FirstOrDefault();
		}

		public Student GetByName(Student student)
		{
			return _coreContext.Student
				.Where(p => p.FullName == student.FullName)
				.FirstOrDefault();
		}

		public void Insert(Student student)
		{
			_coreContext.Student.Add(student);
			_coreContext.SaveChanges();
		}

		public void UpdatePass(Student student)
		{
			var item = GetByID(student);
			if (item is null)
			{
				return;
			}
			item.Pass = student.Pass;
			_coreContext.SaveChanges();
		}
		public void UpdateEmail(Student student)
		{
			var item = GetByID(student);
			if (item is null)
			{
				return;
			}
			item.Email = student.Email;
			_coreContext.SaveChanges();
		}
		public void UpdateLogin(Student student)
		{
			var item = GetByID(student);
			if (item is null)
			{
				return;
			}
			item.Login = student.Login;
			_coreContext.SaveChanges();
		}

		public void UpdateName(Student student)
		{
			var item = GetByID(student);
			if (item is null)
			{
				return;
			}
			item.FullName = student.FullName;
			_coreContext.SaveChanges();
		}

		public void Delete(Student student)
		{
			var item = GetByID(student);
			if (item is null)
			{
				return;
			}
			_coreContext.Student.Remove(item);
			_coreContext.SaveChanges();
		}
	}
}
