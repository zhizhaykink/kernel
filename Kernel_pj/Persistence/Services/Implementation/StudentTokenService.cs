﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreContextes.Models;
namespace EF.Services.Implementation
{
	public class StudentTokenService
	{
		private CoreContext _coreContext;
		public StudentTokenService(CoreContext coreContext)
		{
			_coreContext = coreContext;
		}
		public StudentToken GetByID(int id)
		{
			return _coreContext.StudentToken
				.Where(p => p.ID == id)
				.FirstOrDefault();
		}

		public List<StudentToken> GetByID_Organization(int id)
		{
			return _coreContext.StudentToken
				.Where(p => p.ID_Student == id)
				.ToList();
		}

		public StudentToken GetByGUID(string guid)
		{
			return _coreContext.StudentToken
				.Where(p => p.GUID == guid)
				.FirstOrDefault();
		}

		public void Insert(StudentToken token)
		{
			_coreContext.StudentToken.Add(token);
			_coreContext.SaveChanges();
		}
		public void Delete(int id)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			_coreContext.StudentToken.Remove(item);
			_coreContext.SaveChanges();
		}

		public void Delete(string GUID)
		{
			var item = GetByGUID(GUID);
			if (item is null)
			{
				return;
			}
			_coreContext.StudentToken.Remove(item);
			_coreContext.SaveChanges();
		}
		public DateTime? CheckLive(int id)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return null;
			}
			return item.Live;
		}

		public DateTime? CheckLive(string GUID)
		{
			var item = GetByGUID(GUID);
			if (item is null)
			{
				return null;
			}
			return item.Live;
		}
	}
}
