﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreContextes.Models;
namespace EF.Services.Implementation
{
	public class SupervisorService
	{
		private CoreContext _coreContext;
		public SupervisorService(CoreContext coreContext)
		{
			_coreContext = coreContext;
		}
		public Supervisor GetByID(Supervisor supervisor)
		{
			return _coreContext.Supervisor
				.Where(p => p.ID == supervisor.ID)
				.FirstOrDefault();
		}

		public Supervisor GetByLogin(Supervisor supervisor)
		{
			return _coreContext.Supervisor
				.Where(p => p.Login == supervisor.Login)
				.FirstOrDefault();
		}

		public Supervisor GetByName(Supervisor supervisor)
		{
			return _coreContext.Supervisor
				.Where(p => p.Login == supervisor.FullName)
				.FirstOrDefault();
		}

		public void Insert(Supervisor supervisor)
		{
			_coreContext.Supervisor.Add(supervisor);
			_coreContext.SaveChanges();
		}

		public void UpdatePass(Supervisor supervisor)
		{
			var item = GetByID(supervisor);
			if (item is null)
			{
				return;
			}
			item.Pass = supervisor.Pass;
			_coreContext.SaveChanges();
		}
		public void UpdateEmail(Supervisor supervisor)
		{
			var item = GetByID(supervisor);
			if (item is null)
			{
				return;
			}
			item.Email = supervisor.Email;
			_coreContext.SaveChanges();
		}
		public void UpdateLogin(Supervisor supervisor)
		{
			var item = GetByID(supervisor);
			if (item is null)
			{
				return;
			}
			item.Login = supervisor.Login;
			_coreContext.SaveChanges();
		}

		public void UpdateName(Supervisor supervisor)
		{
			var item = GetByID(supervisor);
			if (item is null)
			{
				return;
			}
			item.FullName = supervisor.FullName;
			_coreContext.SaveChanges();
		}

		public void Delete(Supervisor supervisor)
		{
			var item = GetByID(supervisor);
			if (item is null)
			{
				return;
			}
			_coreContext.Supervisor.Remove(item);
			_coreContext.SaveChanges();
		}
	}
}
