﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreContextes.Models;
namespace EF.Services.Implementation
{
	public class SupervisorTokenService
	{
		private CoreContext _coreContext;
		public SupervisorTokenService(CoreContext coreContext)
		{
			_coreContext = coreContext;
		}
		public SupervisorToken GetByID(int id)
		{
			return _coreContext.SupervisorToken
				.Where(p => p.ID == id)
				.FirstOrDefault();
		}

		public List<SupervisorToken> GetByID_Organization(int id)
		{
			return _coreContext.SupervisorToken
				.Where(p => p.ID_Supervisor == id)
				.ToList();
		}

		public SupervisorToken GetByGUID(string guid)
		{
			return _coreContext.SupervisorToken
				.Where(p => p.GUID == guid)
				.FirstOrDefault();
		}

		public void Insert(SupervisorToken token)
		{
			_coreContext.SupervisorToken.Add(token);
			_coreContext.SaveChanges();
		}
		public void Delete(int id)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			_coreContext.SupervisorToken.Remove(item);
			_coreContext.SaveChanges();
		}
		public DateTime? CheckLive(int id)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return null;
			}
			return item.Live;
		}

		public DateTime? CheckLive(string GUID)
		{
			var item = GetByGUID(GUID);
			if (item is null)
			{
				return null;
			}
			return item.Live;
		}
	}
}
