﻿using CoreContextes.Models;
using EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Services.Implementation
{
	public class TokenService
	{
		private CoreContext _coreContext;
		public TokenService(CoreContext coreContext)
		{
			_coreContext = coreContext;
		}
		public Token GetByID(int id)
		{
			return _coreContext.Token
				.Where(p => p.ID == id)
				.FirstOrDefault();
		}

		public List<Token> GetByID_User(int id, int role)
		{
			return _coreContext.Token
				.Where(p => p.ID_User == id && p.ID_Role == role)
				.ToList();
		}

		public Token GetByGUID(string guid)
		{
			return _coreContext.Token
				.Where(p => p.GUID == guid)
				.FirstOrDefault();
		}

		public void Insert(Token token)
		{
			_coreContext.Token.Add(token);
			_coreContext.SaveChanges();
		}
		public void Delete(int id)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			_coreContext.Token.Remove(item);
			_coreContext.SaveChanges();
		}

		public void Delete(string GUID)
		{
			var item = GetByGUID(GUID);
			if (item is null)
			{
				return;
			}
			_coreContext.Token.Remove(item);
			_coreContext.SaveChanges();
		}
		public TimeSpan? CheckLive(int id)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return null;
			}
			return item.Live;
		}

		public TimeSpan? CheckLive(string GUID)
		{
			var item = GetByGUID(GUID);
			if (item is null)
			{
				return null;
			}
			return item.Live;
		}
	}
}
