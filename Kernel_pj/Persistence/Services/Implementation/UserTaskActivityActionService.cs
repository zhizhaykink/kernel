﻿using CoreContextes.Models;
using EF;
using Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Services.Implementation
{
	public class UserTaskActivityActionService
	{
		private CoreContext _coreContext;
		public UserTaskActivityActionService(CoreContext coreContext)
		{
			_coreContext = coreContext;
		}
		public UserTaskActivityAction GetByID(int id)
		{
			return _coreContext.UserTaskActivityAction
				.Where(p => p.ID == id)
				.FirstOrDefault();
		}


		public List<UserTaskActivityAction> GetByTaskID(int id)
		{
			return _coreContext.UserTaskActivityAction
				.Where(p => p.UserTaskActivityID == id).ToList();
		}

		public void Insert(UserTaskActivityAction userTaskActivityAction)
		{
			_coreContext.UserTaskActivityAction.Add(userTaskActivityAction);
			_coreContext.SaveChanges();
		}
		public void Delete(int id)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			_coreContext.UserTaskActivityAction.Remove(item);
			_coreContext.SaveChanges();
		}

		public void DeleteByTaskID(int id)
		{
			var items = GetByTaskID(id);
			if (items is null)
			{
				return;
			}
			foreach (var item in items)
			{
				_coreContext.UserTaskActivityAction.Remove(item);
			}
			_coreContext.SaveChanges();
		}
	}
}
