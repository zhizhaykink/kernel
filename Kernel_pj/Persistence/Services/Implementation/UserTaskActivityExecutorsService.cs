﻿using CoreContextes.Models;
using EF;
using Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Services.Implementation
{
	public class UserTaskActivityExecutorsService
	{
		private CoreContext _coreContext;
		public UserTaskActivityExecutorsService(CoreContext coreContext)
		{
			_coreContext = coreContext;
		}

		public UserTaskActivityExecutors GetByID(int id)
		{
			return _coreContext.UserTaskActivityExecutors
				.Where(p => p.ID == id)
				.FirstOrDefault();
		}


		public List<UserTaskActivityExecutors> GetByTaskID(int id)
		{
			return _coreContext.UserTaskActivityExecutors
				.Where(p => p.UserTaskActivityID == id).ToList();
		}

		public void Insert(UserTaskActivityExecutors userTaskActivityExecutors)
		{
			_coreContext.UserTaskActivityExecutors.Add(userTaskActivityExecutors);
			_coreContext.SaveChanges();
		}
		public void Delete(int id)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			_coreContext.UserTaskActivityExecutors.Remove(item);
			_coreContext.SaveChanges();
		}

		public void DeleteByTaskID(int id)
		{
			var items = GetByTaskID(id);
			if (items is null || items.Count == 0)
			{
				return;
			}
			foreach (var item in items)
			{
				_coreContext.UserTaskActivityExecutors.Remove(item);
			}
			_coreContext.SaveChanges();
		}
	}
}
