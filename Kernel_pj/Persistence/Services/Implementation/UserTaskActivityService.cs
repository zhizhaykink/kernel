﻿using CoreContextes.Models;
using EF;
using Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Services.Implementation
{
	public class UserTaskActivityService
	{
		private CoreContext _coreContext;
		public UserTaskActivityService(CoreContext coreContext)
		{
			_coreContext = coreContext;
		}
		public UserTaskActivity GetByID(int id)
		{
			return _coreContext.UserTaskActivity
				.Where(p => p.ID == id)
				.FirstOrDefault();
		}

		
		public UserTaskActivity GetByPairExecute(int id)
		{
			return _coreContext.UserTaskActivity
				.Where(p => p.PairForExecuteID == id)
				.FirstOrDefault();
		}

		public void Insert(UserTaskActivity userTaskActivity)
		{
			_coreContext.UserTaskActivity.Add(userTaskActivity);
			_coreContext.SaveChanges();
		}
		public void Delete(int id)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			_coreContext.UserTaskActivity.Remove(item);
			_coreContext.SaveChanges();
		}

		public void UpdateExternalTask(int id, string taskID, string descriptions, int statusID)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			item.ExernalTaskID = taskID;
			item.Descriptions = descriptions;
			item.Status = statusID;
			_coreContext.SaveChanges();
		}

		public void UpdateStatus(int id, int statusID)
		{
			var item = GetByID(id);
			if (item is null)
			{
				return;
			}
			item.Status = statusID;
			_coreContext.SaveChanges();
		}
	}
}
