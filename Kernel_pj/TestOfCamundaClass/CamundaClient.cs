﻿using System;
using System.Collections.Generic;
using System.Threading;
using Camunda.OpenApi.Client.Api;
using Camunda.OpenApi.Client.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CamundaClient
{
	public class CamundaClient
	{
		private readonly ExternalTaskApi client = new ExternalTaskApi();
		private static readonly CancellationTokenSource cts = new CancellationTokenSource();
		public CancellationTokenSource CTS { get { return cts; } }
		public void StartWorker(IActivity activity, string topicName)
		{
			var cancellationToken = cts.Token;
			var thread = new Thread(delegate() 
				{ 
					while (!cancellationToken.IsCancellationRequested) 
						Execute(activity, topicName); 
				}
			);
			thread.Start();
		}

		private void Execute(IActivity activity, string topicName)
		{
			if (Listener(topicName))
			{
				var task = FetchAndLock(topicName);
				var element = JsonConvert.DeserializeObject(CamundaJsonParse.ReParse(System.Text.Json.JsonSerializer.Serialize(task.Variables)), Service.GetTypeActivity(activity));
				Complete(task, activity.Run(element));
			}
		}
		
		private bool Listener(string toicName)
		{
			return client.GetTopicNames(false, true).Contains(toicName);
		}

		private LockedExternalTaskDto FetchAndLock(string topicName)
		{
			var list = new List<FetchExternalTaskTopicDto>
					{
						new FetchExternalTaskTopicDto(topicName, 1000)
					};
			var externalTask = new FetchExternalTasksDto(topicName, 1, true, 100000, list);
			return client.FetchAndLock(externalTask)[0];
		}

		private void Complete(LockedExternalTaskDto task, Dictionary<string, VariableValueDto> outputVariables)
		{
			var completeTaskDto = new CompleteExternalTaskDto
			{
				WorkerId = task.WorkerId,
				Variables = outputVariables
			};
			client.CompleteExternalTaskResource(task.Id, completeTaskDto);
		}
	}

	class FetchAndLockVariableDataInfo
	{
		public string Value { get; set; }
		public string Type { get; set; }
	}

	static class CamundaJsonParse
	{
		public static string ReParse(string json)
		{
			string newJson = "{";
			JObject jObject = JObject.Parse(json);
			foreach (KeyValuePair<string, JToken> obj in jObject)
				newJson += $"\"{obj.Key}\" : \"{obj.Value.ToObject<FetchAndLockVariableDataInfo>().Value}\",";
			if (newJson.Length > 1)
				newJson = newJson.Remove(newJson.Length - 1, 1);
			return newJson + "}";
		}
	}

	static class Service
	{
		static public Type GetTypeActivity(IActivity activity)
		{
			var typeOfRun = activity.GetType().GetMethod("Run");
			var typeOfInputData = typeOfRun.GetParameters()[0].ParameterType;
			return Type.GetType($"{typeOfInputData.Namespace}.{typeOfInputData.Name}, {typeOfInputData.Assembly.GetName()}", false, false);
		}
	}
}
