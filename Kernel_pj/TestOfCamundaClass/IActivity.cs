﻿using Camunda.OpenApi.Client.Model;
using System.Collections.Generic;

namespace CamundaClient
{
	public interface IActivity
	{
		Dictionary<string, VariableValueDto> Run<T>(T obj) where T: class;
	}
}
