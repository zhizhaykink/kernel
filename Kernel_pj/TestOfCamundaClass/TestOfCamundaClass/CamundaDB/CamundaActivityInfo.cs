﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOfCamundaClass
{
    public class CamundaActivityInfo
    {
        private List<СhildActivityInstances> сhildActivity = new List<СhildActivityInstances>();
        public string id { get; set; }
        public string parentActivityInstanceId { get; set; }
        public string activityId { get; set; }
        public string activityType { get; set; }
        public string processInstanceId { get; set; }
        public string processDefinitionId { get; set; }
        public List<СhildActivityInstances> childActivityInstances { get { return сhildActivity; } set { сhildActivity = value; } }
	}
}
