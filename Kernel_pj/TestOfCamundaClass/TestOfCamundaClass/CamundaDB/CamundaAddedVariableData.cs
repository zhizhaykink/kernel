﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOfCamundaClass
{
	public class CamundaAddedVariableData
	{
		private List<string> _processInstanceIds = new List<string>();
		public List<string> processInstanceIds { get { return _processInstanceIds; } set { _processInstanceIds = value; } }
	
		public string variableName { get; set; }
		public string variableValue { get; set; }
	}
}
