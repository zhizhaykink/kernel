﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOfCamundaClass
{
    public class CamundaAddedVariableInfo
    {
        public string id { get; set; }
        public string type { get; set; }
        public int totalJobs { get; set; }
        public int jobsCreated { get; set; }
        public int batchJobsPerSeed { get; set; }
        public int invocationsPerBatchJob { get; set; }
        public string seedJobDefinitionId { get; set; }
        public string monitorJobDefinitionId { get; set; }
        public string batchJobDefinitionId { get; set; }

        public bool suspended { get; set; }
        public string tenantId { get; set; }
        public string createUserId { get; set; }
    }
}
