﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOfCamundaClass
{
	public class CamundaData
	{
		public string DeploymentName { get; set; }
		public bool DeployChangedOnly { get; set; }
		public string DeploymentSource { get; set; }
		public string TenantID { get; set; }
	}
}
