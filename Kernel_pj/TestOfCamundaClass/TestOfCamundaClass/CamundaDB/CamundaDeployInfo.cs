﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOfCamundaClass
{
	public class CamundaDeployInfo
	{
		public string id { get; set; }
		public string key { get; set; }
		public string category { get; set; }
		public string description { get; set; }
		public string name { get; set; }
		public double version { get; set; }
		public string resource { get; set; }
		public string deploymentId { get; set; }
		public string diagram { get; set; }
		public bool suspended { get; set; }
		public string tenantId { get; set; }
		public string versionTag { get; set; }
		public string historyTimeToLive { get; set; }
		public bool startableInTasklist { get; set; }
	}
}
