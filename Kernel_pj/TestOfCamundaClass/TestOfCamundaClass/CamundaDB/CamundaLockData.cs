﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOfCamundaClass
{
	public class CamundaLockData
	{
		private List<Topics> _topics = new List<Topics>();
		public string workerId { get; set; }
		public int maxTasks { get; set; }
		public List<Topics> topics { get { return _topics; } set { _topics = value; } }
	}

	public class Topics
	{
		public string topicName { get; set; }
		public int lockDuration { get; set; }
		public string processDefinitionId { get; set; }

	}
}
