﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOfCamundaClass
{
	public class CamundaLockInfo<T>
    {
		public string activityId { get; set; } 
        public string activityInstanceId { get; set; }
        public string errorMessage { get; set; } 
        public string errorDetails { get; set; }
        public string executionId { get; set; }
        public string id { get; set; }
        public string lockExpirationTime { get; set; }
        public string processDefinitionId { get; set; }
        public string processDefinitionKey { get; set; }
        public string processDefinitionVersionTag { get; set; }
        public string processInstanceId { get; set; }
        public string retries { get; set; }
        public string suspended { get; set; }
        public string workerId { get; set; }
        public string topicName { get; set; }
        public string tenantId { get; set; }
        public string priority { get; set; }
        public string businessKey { get; set; }
        public object? extensionProperties { get; set; }
        public T? variables { get; set; }
    }
}
