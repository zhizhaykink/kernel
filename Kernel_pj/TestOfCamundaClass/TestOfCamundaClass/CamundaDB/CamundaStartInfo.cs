﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOfCamundaClass
{
	public class CamundaStartInfo
    {
	    public string id { get; set; }
        public string definitionId { get; set; }
        public string? businessKey { get; set; }
        public string? caseInstanceId { get; set; }
        public bool ended { get; set; }
        public bool suspended { get; set; }
        public string tenantId { get; set; }
	}
}
