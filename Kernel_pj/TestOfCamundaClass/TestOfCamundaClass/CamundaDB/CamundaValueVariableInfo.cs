﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOfCamundaClass
{
	public class CamundaValueVariableInfo
	{
		public string name { get; set; }
		public string value { get; set; }
		public string type { get; set; }
	}
}
