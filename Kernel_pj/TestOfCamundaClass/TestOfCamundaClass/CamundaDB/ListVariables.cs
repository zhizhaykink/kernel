﻿namespace TestOfCamundaClass
{
	public class ListVariables
	{
		public ListVariables.Action? action { get; set; }
		public Executor? Executors { get; set; }

		public class Action
		{
			public string type { get; set; }
			public string value { get; set; }
		}

		public class Executor
		{
			public string type { get; set; }
			public string value { get; set; }
		}
	}
}