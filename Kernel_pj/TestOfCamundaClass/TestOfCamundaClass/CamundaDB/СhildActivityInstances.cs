﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOfCamundaClass
{
	public class СhildActivityInstances
    {
		public string id { get; set; }
        public string parentActivityInstanceId { get; set; }
        public string activityId { get; set; }
        public string activityType { get; set; }
        public string processInstanceId { get; set; }
        public string processDefinitionId { get; set; }
        public string[] childActivityInstances { get; set; }
        public string[] childTransitionInstances { get; set; }
        public string[] executionIds { get; set; }
        public string activityName { get; set; }
        public string[] incidentIds { get; set; }
        public string[] incidents { get; set; }
        public string name { get; set; }
    }
}
