﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Camunda.Api.Client;
using Camunda.Api.Client.Deployment;
using Newtonsoft.Json;
using TestOfCamundaClass.Handler;

namespace TestOfCamundaClass
{
	public class CamundaService
	{
		private Configuration _configuration;
		private static CamundaClient _camunda;
		public CamundaService(Configuration configuration)
		{
			_configuration = configuration;
			_camunda = CamundaClient.Create($"{_configuration.GetConfiguration.CamundaURL}/engine-rest");
		}

		public CamundaDeployInfo Deploy(Stream file, CamundaData data)
		{
			DeploymentInfo info = _camunda.Deployments.Create(data.DeploymentName, false, data.DeployChangedOnly, data.DeploymentSource, data.TenantID, new ResourceDataContent(file, data.DeploymentName + ".bpmn")).Result;
			Console.WriteLine($"info.Name:     {info.Name}");
			Console.WriteLine($"info.Source:   {info.Source}");
			Console.WriteLine($"info.TenantId: {info.TenantId}");
			Console.WriteLine($"info.Id:       {info.Id}");
			return GetListDefinition(info.Id).Result[0];
		}

		public async Task<CamundaStartInfo> Start(string proccessDefinitionID)
		{
			var client = new HttpClient();
			client.DefaultRequestHeaders
	  .Accept
	  .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
			var stringContent = new StringContent("{\n}", Encoding.UTF8, "application/json");
			HttpResponseMessage response = client.PostAsync($"{_configuration.GetConfiguration.CamundaURL}/engine-rest/process-definition/{proccessDefinitionID}/start", stringContent).Result;
			return JsonConvert.DeserializeObject<CamundaStartInfo>(response.Content.ReadAsStringAsync().Result);
		}

		// ?
		public async Task<CamundaLockInfo<T>> FetchAndLock<T>(CamundaLockData lockData)
		{
			var client = new HttpClient();
			client.DefaultRequestHeaders
	  .Accept
	  .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
			var stringContent = new StringContent($"{JsonConvert.SerializeObject(lockData)}", Encoding.UTF8, "application/json");
			HttpResponseMessage response = client.PostAsync($"{_configuration.GetConfiguration.CamundaURL}/engine-rest/external-task/fetchAndLock", stringContent).Result;
			var locking = JsonConvert.DeserializeObject<List<CamundaLockInfo<T>>>(response.Content.ReadAsStringAsync().Result.ToString());
			return locking[0];
		}

		public async Task Unlock(string id)
		{
			var client = new HttpClient();
			client.DefaultRequestHeaders
	  .Accept
	  .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
			HttpResponseMessage response = client.PostAsync($"{_configuration.GetConfiguration.CamundaURL}/engine-rest/external-task/{id}/unlock", null).Result;
		}

		// ?
		public async void Complete(CamundaCompleteData completeData)
		{
			var client = new HttpClient();
			client.DefaultRequestHeaders
	  .Accept
	  .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
			var stringContent = new StringContent($"{{\n \"workerId\": \"{completeData.WorkerID}\" \n}}", Encoding.UTF8, "application/json");
			HttpResponseMessage response = client.PostAsync($"{_configuration.GetConfiguration.CamundaURL}/engine-rest/external-task/{completeData.ID}/complete", stringContent).Result;
			Console.WriteLine($"{response.RequestMessage.Content.ReadAsStringAsync().Result}");
		}


		// ?
		public async Task<CamundaAddedVariableInfo> AddedVariable(CamundaAddedVariableData variable)
		{
			var client = new HttpClient();
			client.DefaultRequestHeaders
	  .Accept
	  .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
			var stringContent = new StringContent($"{{\n \"processInstanceIds\" : [\n\"{variable.processInstanceIds[0]}\"\n],\n \"variables\": {{\n\"{variable.variableName}\": {{\n\"value\": \"{variable.variableValue}\"\n }}\n}}\n}}", Encoding.UTF8, "application/json");
			HttpResponseMessage response = client.PostAsync($"{_configuration.GetConfiguration.CamundaURL}/engine-rest/process-instance/variables-async", stringContent).Result;
			return JsonConvert.DeserializeObject<CamundaAddedVariableInfo>(response.Content.ReadAsStringAsync().Result);
		}

		// ?
		public async void ChangeVariable(string id, string name, string value)
		{
			var client = new HttpClient();
			client.DefaultRequestHeaders
	  .Accept
	  .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
			var stringContent = new StringContent($"{{\n\"value\": \"{value}\"\n}}", Encoding.UTF8, "application/json");
			HttpResponseMessage response = client.PutAsync($"{_configuration.GetConfiguration.CamundaURL}/engine-rest/process-instance/{id}/variables/{name}", stringContent).Result;
		}


		// ?
		public async Task<CamundaValueVariableInfo> GetVariable(string id, string name)
		{
			var client = new HttpClient();
			client.DefaultRequestHeaders
	  .Accept
	  .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
			HttpResponseMessage response = client.GetAsync($"{_configuration.GetConfiguration.CamundaURL}/engine-rest/process-instance/{id}/variables/{name}").Result;
			return JsonConvert.DeserializeObject<CamundaValueVariableInfo>(response.Content.ReadAsStringAsync().Result);
		}


		// ?
		public async Task<CamundaActivityInfo> GetActivityID(string id)
		{
			var client = new HttpClient();
			client.DefaultRequestHeaders
	  .Accept
	  .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
			HttpResponseMessage response = client.GetAsync($"{_configuration.GetConfiguration.CamundaURL}/engine-rest/process-instance/{id}/activity-instances").Result;
			return JsonConvert.DeserializeObject<CamundaActivityInfo>(response.Content.ReadAsStringAsync().Result);
		}



		public async Task<List<CamundaDeployInfo>> GetListDefinition(string deploymentId)
		{
			var client = new HttpClient();
			client.DefaultRequestHeaders
	  .Accept
	  .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
			HttpResponseMessage response = client.GetAsync($"{_configuration.GetConfiguration.CamundaURL}/engine-rest/process-definition?deploymentId={deploymentId}").Result;
			return JsonConvert.DeserializeObject<List<CamundaDeployInfo>>(response.Content.ReadAsStringAsync().Result);
		}
	}
}
