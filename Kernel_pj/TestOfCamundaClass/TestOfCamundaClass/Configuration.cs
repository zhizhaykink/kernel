﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOfCamundaClass
{
	public class Configuration
	{
		private static Configuration configuration = null;
		
		public Configuration GetConfiguration
		{
			get {
				if (configuration == null)
				{
					configuration = new Configuration();
				}
				return configuration;
			}
			set { configuration = value; }
		}

		public string CamundaURL { get; set; }
		public string LockTime { get; set; }
		public string WorkerTime { get; set; }
		public string Worker { get; set; }
	}
}
