﻿using EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace TestOfCamundaClass.Handler
{
	public class DeployAdapter
	{
		private CamundaService camundaTestService;
		public DeployAdapter(Configuration configuration)
		{
			camundaTestService = new CamundaService(configuration);
		}
		public CamundaDeployInfo Execute(Stream stream, Deploy info)
		{
			var camundaTestConfig = new CamundaData();
			camundaTestConfig.DeploymentName = info.Name;
			camundaTestConfig.DeployChangedOnly = false;
			return camundaTestService.Deploy(stream, camundaTestConfig);
		}
	}
}
