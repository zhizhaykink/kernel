﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOfCamundaClass.Handler
{
	public class ShowDashboardAdapter
	{
		private CamundaService camundaTestService;
		public ShowDashboardAdapter(Configuration configuration)
		{
			camundaTestService = new CamundaService(configuration);
		}
		public List<string> Execute(string id)
		{
			var info = camundaTestService.GetActivityID(id).Result;
			if (info.childActivityInstances.Count == 0)
			{
				throw new KeyNotFoundException("Задача завершена или не запущена");
			}
			var show = info.childActivityInstances[0];
			var lst = new List<string>();
			lst.Add(show.activityName);
			lst.Add(show.activityId);
			return lst;
		}
	}
}
