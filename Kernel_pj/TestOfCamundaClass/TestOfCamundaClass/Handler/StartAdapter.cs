﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOfCamundaClass.Handler
{
	public class StartAdapter
	{
		private CamundaService camundaTestService;
		public StartAdapter(Configuration configuration)
		{
			camundaTestService = new CamundaService(configuration);
		}
		public CamundaStartInfo Execute(string id)
		{
			return camundaTestService.Start(id).Result;
		}
	}
}
