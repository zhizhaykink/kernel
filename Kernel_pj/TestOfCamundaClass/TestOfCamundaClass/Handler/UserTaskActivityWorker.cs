﻿using EF.Services.Implementation;
using Kernel.Entities.Services;
using Microsoft.Extensions.Hosting;
using Persistence.Entities;
using Persistence.Services.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace TestOfCamundaClass.Handler
{
	public class UserTaskActivityWorker : BackgroundService
	{
		private Configuration _configuration;
		private CamundaService service;
		public UserTaskActivityWorker(Configuration configuration)
		{
			_configuration = configuration;
			service = new CamundaService(configuration);
		}
		public void Run()
		{
			while (true)
			{
				try
				{
					async void Start()
					{
						var camundaLockData = new CamundaLockData()
						{
							maxTasks = 1,
							workerId = _configuration.GetConfiguration.Worker
						};
						Topics topics = new()
						{
							topicName = "UserTaskActivity",
							lockDuration = int.Parse(_configuration.GetConfiguration.LockTime)
						};
						List<Topics> topicsList = new();
						topicsList.Add(topics);
						camundaLockData.topics = topicsList;
						var lockedTask = service.FetchAndLock<UserTaskActivityInfo>(camundaLockData);
						if (lockedTask.Status is TaskStatus.Faulted)
						{
							Console.WriteLine("Not found task");
						}
						else
						{
							PairForExecuteService pairForExecuteService = new(new CoreContextes.Models.CoreContext());
							UserTaskActivityService userTaskActivityService = new(new CoreContextes.Models.CoreContext());
							UserTaskActivityActionService userTaskActivityActionService = new(new CoreContextes.Models.CoreContext());
							UserTaskActivityExecutorsService userTaskActivityExecutorsService = new(new CoreContextes.Models.CoreContext());
							var pair = pairForExecuteService.GetByID_ProcessID(lockedTask.Result.processInstanceId);
							if (pair is null || pair.Count == 0)
							{
								Console.WriteLine($"In db not found {lockedTask.Result.processInstanceId}");
								Thread.Sleep(3000);
								service.Unlock(lockedTask.Result.id).Wait();
							}
							else
							{
								Console.WriteLine($"Task is {lockedTask.Result.id} locked {pair.Count} {lockedTask.Result.processInstanceId}");
								UserTaskActivity activity = new();
								activity.ExernalTaskID = lockedTask.Result.id;
								activity.PairForExecuteID = pair[0].ID;
								activity.Descriptions = lockedTask.Result.variables.Description.Value;
								activity.Status = 0;
								var ut = userTaskActivityService.GetByPairExecute(pair[0].ID);
								if (ut is null)
								{
									userTaskActivityService.Insert(activity);
								}
								else
								{
									userTaskActivityService.UpdateExternalTask(ut.ID, activity.ExernalTaskID, activity.Descriptions, 0);
								}
								ut = userTaskActivityService.GetByPairExecute(pair[0].ID);
								UserTaskActivityAction userTaskActivityAction = new();
								userTaskActivityAction.Title = lockedTask.Result.variables.Action.Value;
								userTaskActivityAction.UserTaskActivityID = ut.ID;
								var uta = userTaskActivityActionService.GetByTaskID(userTaskActivityAction.UserTaskActivityID);
								if (uta is not null)
								{
									userTaskActivityActionService.DeleteByTaskID(userTaskActivityAction.UserTaskActivityID);
								}
								userTaskActivityActionService.Insert(userTaskActivityAction);
								UserTaskActivityExecutors userTaskActivityExecutors = new();
								userTaskActivityExecutors.UserTaskActivityID = ut.ID;
								userTaskActivityExecutors.Type = lockedTask.Result.variables.Executors.Value;
								var uts = userTaskActivityExecutorsService.GetByTaskID(userTaskActivityExecutors.UserTaskActivityID);
								if (uts is not null)
								{
									userTaskActivityExecutorsService.DeleteByTaskID(userTaskActivityExecutors.UserTaskActivityID);
								}
								userTaskActivityExecutorsService.Insert(userTaskActivityExecutors);
							}
						}
					}
					Thread.Sleep(int.Parse(_configuration.GetConfiguration.WorkerTime));
					var thread = new Thread(Start);
					thread.Start();
				}
				catch (Exception ex)
				{
					Thread.Sleep(int.Parse(_configuration.GetConfiguration.WorkerTime));
				}
			}
		}

		protected override Task ExecuteAsync(CancellationToken stoppingToken)
		{
			Run();
			return null;
		}
	}

	public class UserTaskActivityInfo
	{
		public Action Action{ get; set; }
		public Executors Executors { get; set; }
		public Description Description { get; set; }
	}

	public class Action
	{
		public string Value { get; set; }
	}

	public class Executors
	{
		public string Value { get; set; }
	}

	public class Description
	{
		public string Value { get; set; }
	}
}
