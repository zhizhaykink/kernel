﻿using EF;
using Kernel.Entities.Exceptions;
using Persistence.Services.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOfCamundaClass.Handler
{
	public class UserTaskManager
	{
		private Configuration _configuration;
		private CamundaService service;
		public UserTaskManager(Configuration configuration)
		{
			_configuration = configuration;
			service = new CamundaService(configuration);
		}
		public Story Complete(UserTaskInput userTaskInput, Persistence.Entities.UserTaskActivity? taskInformation)
		{
			UserTaskActivityService uts= new(new CoreContextes.Models.CoreContext());
			Story story = new();
			if (taskInformation == null) 
			{
				throw new NotFoundException();
			}
			story.Topic = taskInformation.Descriptions;
			story.ActivityInstancedID = taskInformation.ExernalTaskID;
			story.WorkerID = _configuration.GetConfiguration.Worker;
			service.ChangeVariable(userTaskInput.ProcessInstanceID, "changed", userTaskInput.Action);
			CamundaCompleteData completeData = new();
			completeData.WorkerID = _configuration.GetConfiguration.Worker;
			completeData.ID = taskInformation.ExernalTaskID;
			service.Complete(completeData);
			uts.UpdateStatus(taskInformation.ID, 1);
			return story;
		}
	}
}
